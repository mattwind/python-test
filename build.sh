#!/usr/bin/env bash
# 
# Autobuild a python virtual environment for developing apps
# by Matt Wind <mattwind@gmail.com>

# Set some stuff

projectFolder="dev"
pythonVersion="python3"
os=`uname`
packageManager="unknown"

# Detect some stuff

if [ "$os" == "Linux" ]; then
  if [ -f /usr/bin/apt ]; then
    packageManager="apt"
  fi
  if [ -f /usr/bin/yum ]; then
    packageManager="yum"
  fi
fi

if [ "$os" == "Darwin" ]; then
  if [ -f /usr/local/bin/brew ]; then
    packageManager="brew"
  fi
fi

# Ask some stuff

echo
echo "Cool looks like your using $os and your package manager is $packageManager"
read -p "Are you sure you want to continue? " -n 1 -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]
then
  echo "Okay, bye."
  exit
fi

# Functional stuff

configure_with_apt () {
  packageList="python3 python3-pip virtualenv libsnmp-dev snmp-mibs-downloader"
  echo
  echo "We need root permisions to install some packages."
  echo
  if [ ! -f /usr/bin/sudo ]; then
    su -c "apt-get install $pageList -y"
  else
    sudo apt-get install $pageList -y
  fi
}

configure_with_yum () {
  echo "Not working yet.."
  exit
}

configure_with_brew () {
  echo
  echo "If this fails maybe install brew"
  echo "http://docs.python-guide.org/en/latest/starting/install3/osx/"
  echo
  sleep 3
  if [ ! -f /usr/local/bin/python3 ]; then
    echo "Python3 not found, please install it."
    exit
  fi
  if [ ! -f /usr/local/bin/pip3 ]; then
    echo "python3-pip not found please install it."
    exit
  fi
  if [ ! -f /usr/local/bin/virtualenv ]; then
    echo "virtualenv not found please install it."
    exit
  fi
}

# Do stuff

if [ "$packageManager" == "apt" ]; then
  configure_with_apt
fi

if [ "$packageManager" == "yum" ]; then
  configure_with_yum
fi

if [ "$packageManager" == "brew" ]; then
  configure_with_brew
fi

echo "Creating Directory"
mkdir -p $projectFolder

echo "Building Virtual Env"
virtualenv -p $pythonVersion --no-site-packages ./$projectFolder

echo "Activating VirtualEnv"
source "`pwd`/$projectFolder/bin/activate"

echo "Installing Modules"
pip install --upgrade incremental
while read p; do
  pip install $p
done <modules.txt

echo "Done"

cat README.md
