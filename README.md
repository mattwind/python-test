
# Python DevEnv

Build a Virtual Env for developing and testing your Python projects.

## Requirements

* python3
* python3-pip
* virtualenv
* libsnmp-dev 
* snmp-mibs-downloader

## Getting Started

First run the build script, after that you can access the virtual environment by typing `source dev/bin/activate`

To exit the virtual env just type `deactivate`

