import socket
import struct
from umodbus import config
from umodbus.client import tcp

class ModbusUtils:
    
    def __init__(self, ip_address, port, modbus_id):
        self.ip_address = ip_address
        self.port = port
        self.modbus_id = modbus_id
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.ip_address, self.port))

    def close_connection(self):
        self.sock.close()
                    
    def make_float(self, mod_value):
        # convert 2 16 bit words to a float
        newnum = mod_value[0] << 16
        newnum = newnum + mod_value[1]
        # unpack calls binascii.a2b_hex which chokes if odd-length string
        if newnum == 0:
            return 0
        else:
            num = format(newnum,'x')
            float_value = struct.unpack('!f', struct.pack('>HH',mod_value[0],mod_value[1]))[0]
            #float_value = struct.unpack('!f', num.decode('hex'))[0]
            return float_value  

    def make_float_array(self,mod_values):
        print(mod_values)
        float_values = []
        i = 0
        while i < len(mod_values):
            float_value = self.make_float(mod_values[i:i+2])
            print(float_value)
            float_values.append(float_value)
            i = i + 2
        return float_values
     
    def get_system_voltage(self):
        address = 1
        registers = 2
        message = tcp.read_holding_registers(slave_id=self.modbus_id, starting_address=address, quantity=registers)
        response = tcp.send_message(message, self.sock)
        system_voltage = self.make_float(response)
        return system_voltage

    def get_string_currents(self):
        address = 3
        registers = 4 * 2
        message = tcp.read_holding_registers(slave_id=self.modbus_id, starting_address=address, quantity=registers)
        response = tcp.send_message(message, self.sock)
        string_currents = self.make_float_array(response)
        return string_currents
        
    # name, address, number of registers, post_process
    #mod_tuple = ('string_voltage',1,2,'post_process')
