#!/usr/bin/env python

from ModbusUtils import * 
     
ip_address = '172.16.0.90'
port = 502
modbus_id = 31

mutils = ModbusUtils(ip_address, port, modbus_id)

#system_volts = mutils.get_system_voltage()
#print system_volts

string_currents = mutils.get_string_currents()
print(string_currents)

mutils.close_connection()
