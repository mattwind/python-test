#!/usr/bin/env python

from easysnmp import Session

# Create an SNMP session to be used for all our requests
session = Session(hostname='172.16.0.90', community='dpmcro', version=2)

# You may retrieve an individual OID using an SNMP GET
#location = session.get('sysLocation.0')

# You may also specify the OID as a tuple (name, index)
# Note: the index is specified as a string as it can be of other types than
# just a regular integer
#contact = session.get(('sysContact', '0'))

# And of course, you may use the numeric OID too
#description = session.get('.iso.org.dod.internet.mgmt.mib-2.upsMIB.bacs2.bacsObjects.bacsNumStrings.0')

#print description

# Set a variable using an SNMP SET
#session.set('sysLocation.0', 'The SNMP Lab')

# Perform an SNMP walk
system_items = session.walk('.1.3.6.1.4.1.41005.1.1.4') # node table

# Each returned item can be used normally as its related type (str or int)
# but also has several extended attributes with SNMP-specific information
for item in system_items:
    print(item)
